#!/bin/bash

# Checks if user is root to give sudo permissions for regular users
if [ "$(whoami)" == "root" ]; then
  export usesudo=""
else
  export usesudo="sudo "
fi
export whoiam="$(whoami)"

# The project name
export projectname="MIUIAM"

# Main compiler variables
export cdir=/home/miuibrasil/compiler/current # The compiler directory. Change it to the directory you extracted the compiler.
export sourcerom=${cdir}/input/source # Folder for all the source (untouched) ROMs.
export donesource=${cdir}/input/source/done # Folder for all ROMs already processed by the deodex function.
export deodexedrom=${cdir}/input/deodexed # Folder for all Deodedex ROMs ready to be compiled.
export donedeo=${cdir}/input/deodexed/done # Folder for all Deodexed ROMs that were compiled.
export workdir=${cdir}/workdir # Working directory for all compiler functions.
export updatedir=${workdir}/META-INF/com/google/android # Working directory for META-INF files.
export projfiles=${workdir}/project_files # Project dir for temporary files.
export bin=${cdir}/bin # Folder for all binaries used by the compiler for deodexing, extracting and compiling ROMs.
export tools=${bin}/tools # Folder containing all necessary scripts for ROM conversion
export logs=${cdir}/logs # Folder for all logs related to the ROM compiler.
export outputdir=${cdir}/output # Folder for all output files.
export compilerinput=${cdir}/translator/input # Input folder for the translator.
export compileroutput=${cdir}/translator/output # Output folder for the translator.
export reposdir=${cdir}/repositories # Git repositories

# Folder creation
if [[ ! -d ${sourcerom} ]]; then
  mkdir -p ${donesource}
elif [[ ! -d ${deodexedrom} ]]; then
  mkdir -p ${donedeo}
elif [[ ! -d ${workdir} ]]; then
  mkdir -p ${workdir}
elif [[ ! -d ${logs} ]]; then
  mkdir -p ${logs}
elif [[ ! -d ${reposdir} ]]; then
  mkdir -p ${reposdir}
fi

# Binaries variables
export legacydeo="xvfb-run java -jar ${bin}/LODTRTA/Launcher.jar"
export brotli=${bin}/compression/brotli # Brotli compression tool
export p7z=${bin}/compression/7z # 7zip compression tool
export adb=${bin}/androidtools/adb # Android Debug Bridge
export aapt=${bin}/apkdecompress/aapt # Android APK Packaging Tool
export apktool=${bin}/apkdecompress/apktool # Android APK Tool wrapper
export zipalign=${bin}/apkdecompress/zipalign # Android Zip Aligner
export vdexext=${bin}/apkdecompress/vdexExtractor # Android .vdex Deodexer for Oreo ROMs
export make_ext4fs=${bin}/imgtools/make_ext4fs # EXT4 filesystem creator
export ext2simg=${bin}/imgtools/ext2simg # ext to simg converter
export simg2img=${bin}/imgtools/simg2img # simg to img converter
export img2sdat="python ${bin}/imgtools/img2sdat/img2sdat.py" # img to sdat converter
export sdat2img="python ${bin}/imgtools/sdat2img.py" # sdat to img converter
export zipsigner="java -jar ${bin}/signer/signapk.jar ${bin}/signer/miuibrasil.x509.pem ${bin}/signer/miuibrasil.pk8" # Jar file to sign ROMs using MIUI Brasil private key for future OTA releases.
export verityoff="python3.6 ${bin}/kerneltools/rmverity.py" # Remove DM-Verity on Oreo Kernel Images
export getmetadata="python3.6 ${bin}/imgtools/getmeta.py" # Get metadata from Filesystem
export translator="java -Xmx4096m -jar ${cdir}/translator/jbart3h.jar" # jBart compiler

# Compiler variables
export maindevice="gemini" # The main device to extract translations from
