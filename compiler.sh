#!/bin/bash
#MIUI ROM Compiler
#Created by Matheus Ferreira

source config.sh

# Colorized output
if which 'tput' > /dev/null; then
  # do we have a terminal?
  if [ -t 1 ]; then
    # does the terminal have colors?
    ncolors=$(tput colors)
    if [ "$ncolors" -ge 8 ]; then
      RED=$(tput setaf 1)
      GREEN=$(tput setaf 2)
      YELLOW=$(tput setaf 3)
      BLUE=$(tput setaf 4)
      MAGENTA=$(tput setaf 5)
      CYAN=$(tput setaf 6)
      WHITE=$(tput setaf 7)
      REDBG=$(tput setab 1)
      GREENBG=$(tput setab 2)
      YELLOWBG=$(tput setab 3)
      BLUEBG=$(tput setab 4)
      MAGENTABG=$(tput setab 5)
      CYANBG=$(tput setab 6)
      WHITEBG=$(tput setab 7)

      BOLD=$(tput bold)
      UNDERLINE=$(tput smul) # Many terminals don't support this
      NORMAL=$(tput sgr0)
    fi
  fi
else
  echo "tput not found, colorized output disabled."
  RED=''
  GREEN=''
  YELLOW=''
  BLUE=''
  MAGENTA=''
  CYAN=''
  WHITE=''
  REDBG=''
  GREENBG=''
  YELLOWBG=''
  BLUEBG=''
  MAGENTABG=''
  CYANBG=''

  BOLD=''
  UNDERLINE=''
  NORMAL=''
fi

# Warning for directory spaces
if [[ $(echo $(pwd) | grep " ") ]]; then
  header
	echo -e "${RED}This compiler must be called in a path without spaces.${NORMAL}\n\n${BLUE}CURRENT PATH:${NORMAL}"
	echo "${YELLOW}$(pwd)${NORMAL}"
  footer
	exit
fi

# Root privileges check
check_user(){
  if [ "$(whoami)" == "root" ]; then
    export usesudo=""
  else
    export usesudo="sudo "
  fi
  export whoiam="$(whoami)"
}

# Checks for missing binaries that are needed for the compiler to work properly
check_deps(){
  if [ -z "$(which axel)" ]; then
    MISSING_DEPS="${MISSING_DEPS}axel "
  fi
  if [ -z "$(which java)" ]; then
    MISSING_DEPS="${MISSING_DEPS}java "
  fi
  if [ -z "$(which gawk)" ]; then
    MISSING_DEPS="${MISSING_DEPS}gawk "
  fi
  if [ -z "$(which file)" ]; then
    MISSING_DEPS="${MISSING_DEPS}file "
  fi
  if [ -z "$(which zip)" ]; then
    MISSING_DEPS="${MISSING_DEPS}zip "
  fi
  if [ -z "$(which unzip)" ]; then
    MISSING_DEPS="${MISSING_DEPS}unzip "
  fi
  if [ -z "$(which rar)" ]; then
    MISSING_DEPS="${MISSING_DEPS}rar "
  fi
  if [ -z "$(which unrar)" ]; then
    MISSING_DEPS="${MISSING_DEPS}unrar "
  fi
  if [ -z "$(which nano)" ]; then
    MISSING_DEPS="${MISSING_DEPS}nano "
  fi
  if [ -z "$(which lzop)" ]; then
    MISSING_DEPS="${MISSING_DEPS}lzop "
  fi
  if [ -z "$(which cpio)" ]; then
    MISSING_DEPS="${MISSING_DEPS}cpio "
  fi
  if [ -z "$(which xvfb-run)" ]; then
    MISSING_DEPS="${MISSING_DEPS}xvfb-run "
  fi
  if [ -n "${MISSING_DEPS}" ]; then
    clear; echo "Your computer is missing the following programs or libraries:"; echo ""; echo "${RED}${MISSING_DEPS}${NORMAL}"; echo ""; echo "Install them using your distribution's package manager."; exit
  fi
}

# System cleanup
cleanup(){
  echo "Cleaning up..."
  ${usesudo}umount ${workdir}/output 2>/dev/null
  ${usesudo}rm -rf ${workdir}/* ${workdir}/.cache/* ${cdir}/translator/projects/* ${deodexedrom}/build.prop
}

# Menu header
header(){
  clear
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
  COLUMNS=$(tput cols)
  htitle="MIUI.AM ROM Kitchen - $(date +%d/%m/%Y) - http://miui.am"
  printf "%*s\n" $(((${#htitle}+$COLUMNS)/2)) "$htitle"
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
  echo "Current user: ${GREEN}$(whoami)${NORMAL}"
}

# Menu footer
footer(){
  echo ""
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
}

# Main menu
mainmenu(){
  header
  echo "Main Menu:"
  echo ""
  echo "${GREEN}1.${NORMAL} Download ROMs"
  echo "${GREEN}2.${NORMAL} Deodex ROMs"
  echo "${GREEN}3.${NORMAL} Extract English XMLs"
  echo "${GREEN}4.${NORMAL} Extract Global XMLs"
  echo "${GREEN}5.${NORMAL} Create patchable APK Packs"
  echo "${GREEN}6.${NORMAL} Update repositories"
  echo "${GREEN}7.${NORMAL} Update translations"
  echo "${GREEN}8.${NORMAL} Compile ROMs"
  echo "${GREEN}9.${NORMAL} Upload ROMs"
  echo "${RED}0.${NORMAL} Exit"
  footer
  echo -n "Select option: "
  read -r choice
  case ${choice} in
    1) download_roms; unset choice; mainmenu;;
    2) deodex_roms; unset choice; mainmenu;;
    3) extract_english; unset choice; mainmenu;;
    4) extract_global; unset choice; mainmenu;;
    5) create_pack; unset choice; mainmenu;;
    6) update_repositories; unset choice; mainmenu;;
    7) update_translations; unset choice; mainmenu;;
    8) compile_roms; unset choice; mainmenu;;
    9) upload_roms; unset choice; mainmenu;;
    0) clear; exit;;
  esac
}

# Compile ROMs
compile_roms(){
  cd ${deodexedrom} || exit
  for zipfile in *.zip; do
    cleanup
    header
    echo "Selected option: ${GREEN}Compile ROMs${NORMAL}"
    echo "Current file: ${GREEN}${zipfile}${NORMAL}"
    footer
    isglobal="$(echo ${zipfile} | grep Global)"; export isglobal
    if [[ ${isglobal} != "" ]]; then
      export basetype="global"
    else
      export basetype="china"
    fi
    unzip -qj ${zipfile} system/build.prop
    if [[ -f ${deodexedrom}/build.prop ]]; then
      devicename2="$(cat ${deodexedrom}/build.prop | grep ro.product.device= | cut -d "=" -f2)"
      romdevice="$(echo ${zipfile} | cut -d '_' -f2)"; export romdevice
      romversion="$(echo ${zipfile} | cut -d '_' -f3)"; export romversion
      rombase="$(echo ${zipfile} | cut -d '_' -f5 | cut -c1-3)"; export rombase
      if [[ -d ${outputdir}/${romversion} ]]; then
        echo "Output folder prepared."
      else
        echo "Creating output folder..."
        mkdir -p ${outputdir}/${romversion}
      fi
    else
      echo "Your ROM couldn't be recognized by the compiler."
      sleep 1
      return
    fi
    mv ${zipfile} ${compilerinput}/input.zip
    if [[ "$(hostname)" != "miuibrasil" ]]; then
      ${usesudo}hostname miuibrasil
    fi
    echo "Translating ROM, please wait..."
    ${translator} config ${cdir}/translator/data/settings/${basetype}.conf
    echo "Signing output zip and moving translated ROM to the output folder..."
    ${zipsigner} ${compileroutput}/output.zip ${outputdir}/${romversion}/output_signed.zip
    rm -rf ${compileroutput}/output.zip
    echo "Generating md5..."
    rom_md5="$(md5sum ${outputdir}/${romversion}/output_signed.zip | cut -d " " -f1 | cut -c1-10)"
    mv ${outputdir}/${romversion}/output_signed.zip ${outputdir}/${romversion}/${projectname}_${romdevice}_${romversion}_${rom_md5}_${rombase}.zip
    echo "Moving source ROM back into folder..."
    mv ${compilerinput}/input.zip ${donedeo}/${zipfile}
    mv ${cdir}/translator/logs/compiler-input.zip.log ${cdir}/translator/logs/compiler-${romdevice}.log
    mv ${cdir}/translator/logs/decompiler-input.zip.log ${cdir}/translator/logs/decompiler-${romdevice}.log
    echo "Translation for ${romdevice} is now complete."
    unset devicename2; unset romdevice; unset romversion; unset rombase; unset rom_md5
    cd ${deodexedrom} || exit
    rm -rf build.prop
  done
  echo "All ROMs compiled. You sdhould find them in ${outputdir}."
}

# Extract English XMLs
extract_english(){
  cd ${deodexedrom} || exit
  for zipfile in *.zip; do
    cleanup
    header
    echo "Selected option: ${GREEN}Extract English XMLs${NORMAL}"
    echo "Current file: ${GREEN}${zipfile}${NORMAL}"
    footer
    mkdir -p ${workdir}/.cache/ROM/system
    echo "Extracting apps..."
    unzip -qj ${zipfile} *.apk system/build.prop -d ${workdir}/.cache/ROM/system
    cd ${workdir}/.cache/ROM || exit
    devname="$(cat system/build.prop | grep ro.product.device= | cut -d "=" -f2)"; export devname
    echo "Installing frameworks..."
    ${apktool} if ${workdir}/.cache/ROM/system/framework-res.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/framework-ext-res.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/miui.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/miuisystem.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/PersonalAssistantPlugin.apk >>/dev/null
    if [[ -f ${tools}/devices/${devname} ]]; then
      cat ${tools}/devices/${devname} | while read apk; do
        apk_name="$(basename ${apk})"; export apk_name
        if [[ -e ${workdir}/.cache/ROM/system/${apk} ]]; then
          rm -rf ${workdir}/.cache/apk_wip
          echo "Decoding ${GREEN}${apk_name}${NORMAL}..."
          ${apktool} decode -f -s ${workdir}/.cache/ROM/system/${apk} -o ${workdir}/.cache/apk_wip >>/dev/null
          cat ${tools}/devices/xmltypes | while read xmltype; do
            if [[ -e ${workdir}/.cache/apk_wip/res/values/${xmltype}.xml ]]; then
              sed -i '/APKTOOL_DUMMY/d' ${workdir}/.cache/apk_wip/res/values/${xmltype}.xml
              if [[ ${devname} == ${maindevice} ]]; then
                mkdir -p ${outputdir}/XMLs/Source/main/${apk_name}/res/values
                cp ${workdir}/.cache/apk_wip/res/values/${xmltype}.xml ${outputdir}/XMLs/Source/main/${apk_name}/res/values
              else
                mkdir -p ${outputdir}/XMLs/Source/device/${devname}/${apk_name}/res/values
                cp ${workdir}/.cache/apk_wip/res/values/${xmltype}.xml ${outputdir}/XMLs/Source/device/${devname}/${apk_name}/res/values
              fi
            fi
          done
        fi
      done
    else
      echo "No apps found for selected device."
    fi
    cd ${deodexedrom} || exit
  done
}

# Extract Global XMLs
extract_global() {
  cd ${deodexedrom} || exit
  for zipfile in *Global*.zip; do
    cleanup
    header
    echo "Selected option: ${GREEN}Extract Global XMLs${NORMAL}"
    echo "Current file: ${GREEN}${zipfile}${NORMAL}"
    footer
    mkdir -p ${workdir}/.cache/ROM/system
    unzip -qj ${zipfile} *.apk system/build.prop -d ${workdir}/.cache/ROM/system
    cd ${workdir}/.cache/ROM || exit
    devname="$(cat system/build.prop | grep ro.product.device= | cut -d "=" -f2)"; export devname
    echo "Installing device frameworks..."
    ${apktool} if ${workdir}/.cache/ROM/system/framework-res.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/framework-ext-res.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/miui.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/miuisystem.apk >>/dev/null
    ${apktool} if ${workdir}/.cache/ROM/system/PersonalAssistantPlugin.apk >>/dev/null
    localelist="$(cat ${tools}/langcodes)"; export localelist
    if [[ -f ${tools}/devices/${devname} ]]; then
      cat ${tools}/devices/${devname} | while read apk; do
        apk_name="$(basename ${apk})"; export apk_name
        if [[ -e ${workdir}/.cache/ROM/system/${apk} ]]; then
          rm -rf ${workdir}/.cache/apk_wip
          echo "Decoding ${GREEN}${apk_name}${NORMAL}..."
          ${apktool} decode -f -s ${workdir}/.cache/ROM/system/${apk} -o ${workdir}/.cache/apk_wip >>/dev/null
          cat ${tools}/devices/xmltypes | while read xmltype; do
            for locale in ${localelist}; do
              if [[ -e ${workdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ]]; then
                sed -i '/APKTOOL_DUMMY/d' ${workdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml
                if [[ ${devname} == ${maindevice} ]]; then
                  mkdir -p ${outputdir}/XMLs/Global/main/${apk_name}/res/values${locale}
                  cp ${workdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ${outputdir}/XMLs/Global/main/${apk_name}/res/values${locale}
                else
                  mkdir -p ${outputdir}/XMLs/Global/device/${devname}/${apk_name}/res/values${locale}
                  cp ${workdir}/.cache/apk_wip/res/values${locale}/${xmltype}.xml ${outputdir}/XMLs/Global/device/${devname}/${apk_name}/res/values${locale}
                fi
              fi
            done
          done
        fi
      done
    else
      echo "No apps found for selected device."
    fi
    cd ${deodexedrom} || exit
  done
}

# Download ROMs - Menu item 1
download_roms(){
  header
  echo "Selected option: ${GREEN}Download ROMs${NORMAL}"
  echo ""
  echo "Please have ALL ROM URLs ready to be pasted on the next screen."
  echo "The ${BLUE}nano${NORMAL} text editor will be opened."
  echo "To save the file, press CTRL+O after pasting all URLS."
  echo "To exit, press CTRL+X after saving the file."
  echo "Add only one URL per line."
  footer
  echo -n "Press c to continue, or b to go back to the main menu: "
  read -r submenu
  case ${submenu} in
    c|C)
    header
    echo "Selected option: ${GREEN}Download ROMs${NORMAL}"
    footer
     # Changes into the source zip directory
     cd ${sourcerom} || exit
     # Opens up nano text editor to add ROM URLs for download
     nano romlist
     # If the romlist file is present on the source directory, proceed to check it's contents and download everything.
     if [[ -f romlist ]]; then
       # For each URL present on the romlist file, call axel to download the file
       for romurl in $(<romlist); do
         echo "Downloading ${romurl}..."
         axel -a -n5 ${romurl} >> /dev/null
       done
       # Remove romlist after downloading all ROMs.
       rm -rf romlist
       echo -e "\nAll ROM Files downloaded."
       read -n1 -r -p "Press any key to continue..."
       unset submenu
     # If the romlist file isn't present, assume that user didn't inserted URLs properly and halt the process.
     else
       echo -e "The ${RED}romlist${NORMAL} file wasn't found.\nIt's necessary for the download script to work."
       echo -e "Please execute the download option again and don't forget to add the URLs.\n"
       echo -e "Read the inscructions carefully to avoid errors during the process."
       read -n1 -r -p "Press any key to continue..."
     fi
    ;;
    b|B) unset submenu;;
  esac
}

# Deodex ROMs - Menu item 2
deodex_roms(){
  header
  echo "Selected option: ${GREEN}Deodex ROMs${NORMAL}"
  footer
  cd ${sourcerom} || exit
  rm -rf ${logs}/*
  for sourcezip in *.zip; do
    logname="$(echo "${sourcezip}" | cut -d "_" -f2)"
    versioninfo="$(echo "${sourcezip}" | cut -d "_" -f3)"
    androidinfo="$(echo "${sourcezip}" | cut -d "_" -f5 | cut -d "." -f1-2)"
    cleanup
    extract_new
    sepolicy_patch
    deodex_start
    zipalign_apps
    build_zip
    finish_build
  done
}

# Finish build
finish_build(){
  cd ${workdir} || exit
  echo "Moving deodexed zip into the compiler folder..."
  mv output.zip ${deodexedrom}/miui_${logname}_${versioninfo}_deodexed_${androidinfo}.zip
  cd ${sourcerom} || exit
  mv ${sourcezip} done/${sourcezip}
  cleanup
}

# Build zip
build_zip(){
  cd ${workdir} || exit
	exzipfiles="*.img *.bin META-INF firmware-update install system vendor" # firmware-update removed
  echo "Building deodexed zip..."
  ${p7z} a -tzip -mx5 output.zip ${exzipfiles} 2>/dev/null >> ${logs}/deodex-${logname}.log
}

# Zipalign apps
zipalign_apps() {
	cd ${workdir} || exit
	find system vendor -name *.apk 2>/dev/null | grep -v "/framework/" | sed 's/^\.\///' | sort | while read line; do
		app=$(basename ${line})
		${zipalign} -f 4 ${line} ${line}-2 >/dev/null 2>&1
		if [[ -f ${line}-2 ]]; then
			mv ${line}-2 ${line}
		fi
	done
	find system/framework -type f | grep .apk | grep -v "${projfiles}" | sed 's/^\.\///' | while read line; do
		app=$(basename ${line})
		${zipalign} -f 4 ${line} ${line}-2 >/dev/null 2>&1
		if [[ -f ${line}-2 ]]; then
			mv ${line}-2 ${line}
		fi
	done
}

# Deodex ROMs - Main deodex script
deodex_start(){
  cd ${workdir} || exit
  if [[ -f ${sysdir}/odex.sqsh ]]; then
    sqshfile="odex.sqsh"
    deodex_sqsh
    if [[ -f ${sysdir}/odex1.sqsh ]]; then
      sqshfile="odex1.sqsh"
      deodex_sqsh
      if [[ -f ${sysdir}/odex2.sqsh ]]; then
        sqshfile="odex2.sqsh"
        deodex_sqsh
      fi
    fi
  elif [[ -f ${sysdir}/odex.app.sqsh ]]; then
		sqshfile="odex.app.sqsh"
		deodex_sqsh
		if [[ -f ${sysdir}/odex.priv-app.sqsh ]]; then
			sqshfile="odex.priv-app.sqsh"
			deodex_sqsh
		fi
		if [[ -f ${sysdir}/odex.framework.sqsh ]]; then
			sqshfile="odex.framework.sqsh"
			deodex_sqsh
		fi
		if [[ -f ${sysdir}/etc/product/orig.applications.sqsh ]]; then
			sqshfile="orig.applications.sqsh"
			deodex_sqsh
		fi
    grep -v "odex.app\|odex.priv-app\|odex.framework\|orig.applications" ${updatedir}/updater-script > ${updatedir}/updater-script2
		mv ${updatedir}/updater-script2 ${updatedir}/updater-script
		grep -v "odex.app\|odex.priv-app\|odex.framework\|orig.applications" ${projfiles}/symlinks > ${projfiles}/symlinks2
		mv ${projfiles}/symlinks2 ${projfiles}/symlinks
		grep -v "odex.app\|odex.priv-app\|odex.framework\|orig.applications" ${projfiles}/symlinks.orig > ${projfiles}/symlinks.orig2
		mv ${projfiles}/symlinks.orig2 ${projfiles}/symlinks.orig
  fi
  get_heapsize
  if [[ ${api} -eq "21" || ${api} -eq "22" ]]; then
    oat2dex=$(find ${bin}/apkdecompress -name *oat2dex*)
    dtype="l"
    deodex
  elif [[ ${api} -eq 23 && ! ${androidversion} = "N" ]]; then
    smali=$(find ${bin}/apkdecompress -name *smali* | grep -v "baksmali")
    baksmali=$(find ${bin}/apkdecompress -name *baksmali*)
    dtype="m2"
    deodex
  elif [[ ${api} -eq 23 || ${api} -eq 24 || ${api} -eq 25 ]] && [[ ${androidversion} != "O" ]]; then
    smali=$(find ${bin}/apkdecompress -name *smali* | grep -v "baksmali")
    baksmali=$(find ${bin}/apkdecompress -name *baksmali*)
    dtype="n2"
    deodex
  elif [[ ${api} -ge 25 ]]; then
    dtype="o"
    deodex
  else
    echo "The ROM file supplied for deodex isnt compatible with this compiler."
    echo "Please try again."
    echo "ROM supplied: ${sourcezip}"
    exit
  fi
}
deodex() {
	arch=""
	arch2=""
	while read i; do
		if [[ -d "${framedir}/${i}" ]]; then
            archtest="${i}"
            break
        fi
	done <<< "$(echo -e "arm64\nx86_64\narm\nx86")"
	if [[ $(echo "${archtest}" | grep "64") ]]; then
		arch2test=$(echo "${archtest}" | sed 's/_64//; s/64$//')
	fi
	if [[ ${archtest} && -f ${framedir}/${archtest}/boot.oat ]]; then
		arch="${archtest}"
		if [[ ${arch2test} && -f ${framedir}/${arch2test}/boot.oat ]]; then
			arch2="${arch2test}"
		fi
	fi
	if [[ ! -f ${framedir}/${arch}/boot.oat ]]; then
		echo "${RED}WARNING:${NORMAL}"
		echo "There is no boot.oat in this rom. It cannot be deodexed, or is already deodexed."
    return
	fi
	dodeodex() {
		app=""
		for app in $(echo "${applist}"); do
			cd "${deoappdir}/${app}" || exit
        	app2=$(basename "$(find -name *.apk 2>/dev/null | head -n 1 | sed 's/\.apk$//')" 2>/dev/null)
			if [[ ! -d ${deoarch} || ! -f ${app2}.apk ]]; then
				continue
			fi
			if [[ ! $(${p7z} l ${app2}.apk | grep classes.dex) ]]; then
				echo -e "Deodexing ${GREEN}${app}${NORMAL}...\n"
				if [[ ${dtype} = "m2" || ${dtype} = "n2" ]]; then
					classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch}/${app2}.odex 2>&1)
					echo "${classes}" >> ${logs}/deodex-${logname}.log
					echo "${classes}" | while read line; do
						apkdex=$(basename $(echo "${line}"))
						if [[ ! $(echo "${apkdex}" | grep classes) ]]; then
							dexclass="classes.dex"
						else
							dexclass=$(echo "${apkdex}" | cut -d":" -f2-)
						fi
						java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${framedir}/${deoarch2}/boot.oat ${deoarch}/${app2}.odex/${apkdex} -o ${deoarch}/smali >> ${logs}/deodex-${logname}.log
						java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} ${deoarch}/smali -o ${deoarch}/${dexclass} >> ${logs}/deodex-${logname}.log
						rm -rf ${deoarch}/smali
						if [[ ! -f ${deoarch}/${dexclass} ]]; then
							echo "${deoappdir/$workdir/}/${app}/${deoarch}/${dexclass}" >> ${logs}/deodex_fail-${logname}.log
							continue
						fi
					done
				elif [[ ${dtype} = "l" ]]; then
					java -Xmx${heapsize}m -jar ${oat2dex} ${deoarch}/${app2}.odex ${framedir}/${deoarch2}/odex >> ${logs}/deodex-${logname}.log
					if [[ ! -f ${deoarch}/${app2}.dex ]]; then
						rm -rf ${deoarch}/${app2}-classes*.dex
						continue
					fi
					mv ${deoarch}/${app2}.dex ${deoarch}/classes.dex
					find ${deoarch} -name ${app2}-classes*.dex | while read line; do
						appclassdex=$(basename ${line})
						appclassdir=$(dirname ${line})
						classname=$(echo "${appclassdex}" | cut -d"-" -f2)
						mv ${line} ${appclassdir}/${classname}
					done
				elif [[ ${dtype} = "o" ]]; then
					cd ${deoarch} || exit
					${vdexext} -i ${app2}.vdex >> ${logs}/deodex-${logname}.log
					if [[ ! -f ${app2}.apk_classes.dex ]]; then
						${vdexext} -i ${app2}.vdex --ignore-crc-error >> ${logs}/deodex-${logname}.log
						if [[ ! -f ${app2}.apk_classes.dex ]]; then
							echo "${deoappdir/$workdir/}/${app}/${app2}.apk" >> ${logs}/deodex_fail-${logname}.log
							rm -rf ${app2}.apk_classes*
							continue
						else
							echo "${deoappdir/$workdir/}/${app}/${app2}.apk" >> ${logs}/deodex_crc_ignored-${logname}.log
						fi
					fi

					ls | grep ".apk_classes" | while read i; do
						mv "${i}" "$(echo ${i##*_})"
					done
					cd ${deoappdir}/${app} || exit
				fi
				(${aapt} add -fk "${app2}.apk" "${deoarch}"/classes*.dex 2>&1) >> ${logs}/deodex-${logname}.log
				if [[ ${dtype} = "l" ]]; then
					rm -rf ${deoarch}
				else
					rm -rf oat
				fi
			else
				echo -e "${GREEN}${app}${NORMAL} is already deodexed...\n"
				if [[ ${dtype} != "l" ]]; then
					rm -rf ${deoappdir}/${app}/oat
				else
					rm -rf ${deoappdir}/${app}/${deoarch}
				fi
			fi
		done
	}
	rm -rf ${projfiles}/deodex_*
	touch ${projfiles}/deodex_$dtype
	if [[ $(find ${appdir} ${privdir} ${framedir} -name *odex.* 2>/dev/null | grep ".gz\|.xz") ]]; then
		echo -e "Extracting odex files...\n"
		find ${appdir} ${privdir} ${framedir} -name *odex.gz 2>/dev/null | while read line; do
			gzdir=$(dirname ${line})
			gzfile=$(basename ${line})
			echo -e "Extracting ${GREEN}${gzfile}${NORMAL}...\n"
			${p7z} e -o${gzdir} ${line} 2>/dev/null >> ${logs}/deodex-${logname}.log
		done
		find ${appdir} ${privdir} ${framedir} -name *odex.xz 2>/dev/null | while read line; do
			xzdir=$(dirname ${line})
			xzfile=$(basename ${line})
			echo -e "Extracting ${GREEN}${xzfile}${NORMAL}...\n"
			${p7z} e -o${xzdir} ${line} 2>/dev/null >> ${logs}/deodex-${logname}.log
		done
	fi
	if [[ ${dtype} != "l" ]]; then
		if [[ -f ${workdir}/system/init.rc && -d ${workdir}/system/system/app ]]; then
			odextmp=$(find system vendor -name *.odex 2>/dev/null | grep -v "system/system/framework/oat/\|system/system/framework/${arch}/\|system/system/framework/${arch2}/\|^system/system/app/\|^system/system/priv-app/\|system/system/vendor/framework" | rev | cut -d"/" -f4- | rev | sort -u)
		else
			odextmp=$(find system vendor -name *.odex 2>/dev/null | grep -v "system/framework/oat/\|system/framework/${arch}/\|system/framework/${arch2}/\|^system/app/\|^system/priv-app/\|system/vendor/framework" | rev | cut -d"/" -f4- | rev | sort -u)
		fi
	else
		odextmp=$(find system vendor -name *.odex 2>/dev/null | grep -v "system/framework/${arch}/\|system/framework/${arch2}/\|^system/app/\|^system/priv-app/" | rev | cut -d"/" -f3- | rev | sort -u)
	fi
	if [[ -d ${sysdir}/app && ${odextmp} ]]; then
		echo -e "Moving extra apps...\n"
		extraapp=""
		line=""
		for line in ${odextmp}; do
			if [[ $(basename "${line}" | grep "^\.") ]]; then
				newapp=$(ls ${line} | grep .apk | cut -d"." -f1)
				newappdir=$(echo "${line}" | rev | cut -d"/" -f2- | rev)/${newapp}
				mv ${line} ${newappdir}
				echo "${workdir}/${newappdir} ${workdir}/${line}" >> ${logs}/extramv-${logname}.txt
				line2=$(echo ${line} | sed 's:\/:\\/:g')
				newappdir2=$(echo ${newappdir} | sed 's:\/:\\/:g')
				line="${newappdir}"
				extraapp=$(basename ${line})
			else
				extraapp=$(basename ${line})
				echo "${sysdir}/app/${extraapp} ${workdir}/${line}" >> ${logs}/extramv-${logname}.txt
			fi
			mv ${workdir}/${line} ${sysdir}/app/${extraapp}
		done
	fi
	chimerao=$(find ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera -name *.odex 2>/dev/null | grep "${arch}")
	chimerav=$(find ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera -name *.vdex 2>/dev/null | grep "${arch}")
	if [[ ${chimerao} ]]; then
		for i in $(echo "${chimerao}"); do
			aname=$(basename ${i} | sed 's/\.odex//')
			dname=$(echo "${i}" | rev | cut -d"/" -f4)
			mkdir -p ${sysdir}/app/${aname}/oat/${arch}
			mv ${i} ${sysdir}/app/${aname}/oat/${arch}/
			if [[ ${chimerav} ]]; then
				if [[ -f $(echo "${i}" | sed 's/\.odex$/\.vdex/') ]]; then
					mv $(echo "${i}" | sed 's/\.odex$/\.vdex/') ${sysdir}/app/${aname}/oat/${arch}/
				fi
			fi
			mv ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera/${dname}/${aname}.apk ${sysdir}/app/${aname}/
			echo "${sysdir}/app/${aname}/${aname}.apk ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera/${dname}/" >> ${logs}/extramv-${logname}.txt
		done
		rm -rf ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera/${dname}/oat
	fi
	if [[ -d "${sysdir}/vendor/framework" ]]; then
		vframe=$(find ${sysdir}/vendor/framework -name *.jar 2>/dev/null)
		for i in ${vframe}; do
			nframe=$(basename ${i} | sed 's/\.jar$//')
			dframe=$(dirname ${i})
			flip=""
			if [[ -f ${dframe}/oat/${arch}/${nframe}.odex ]]; then
				mv ${dframe}/oat/${arch}/${nframe}.odex ${framedir}/oat/${arch}/
				flip="1"
			fi
			if [[ -f ${dframe}/oat/${arch}/${nframe}.vdex ]]; then
				mv ${dframe}/oat/${arch}/${nframe}.vdex ${framedir}/oat/${arch}/
				flip="1"
			fi
			if [[ ${flip} ]]; then
				mv ${i} ${framedir}/
				echo "${framedir}/${nframe}.jar ${i}" >> ${logs}/extramv-${logname}.txt
			fi
		done
		rm -rf ${sysdir}/vendor/framework/oat
	fi
	if [[ ${dtype} = "l" ]]; then
		echo -e "Deoptimizing boot.oat...\n"
		if [[ ! -d "${framedir}/${arch}/odex" ]]; then
			java -Xmx${heapsize}m -jar ${oat2dex} boot ${framedir}/${arch}/boot.oat >> ${logs}/deodex-${logname}.log
		fi
		if [[ ${arch2} ]]; then
			if [[ ! -d "${framedir}/${arch2}/odex" ]]; then
				java -Xmx${heapsize}m -jar ${oat2dex} boot ${framedir}/${arch2}/boot.oat >> ${logs}/deodex-${logname}.log
			fi
		fi
	fi
	echo -e "Starting deodex on ${BLUE}system/app${NORMAL}...\n"
	applist=$(ls ${appdir})
	app=""
	for app in $(echo "${applist}"); do
		if [[ ${dtype} = "l" ]]; then
			if [[ ${arch2} && -d ${appdir}/${app}/${arch} && -d ${appdir}/${app}/${arch2} ]]; then
				rm -rf ${appdir}/${app}/${arch2}
			fi
		else
			if [[ ${arch2} && -d ${appdir}/${app}/oat/${arch} && -d ${appdir}/${app}/oat/${arch2} ]]; then
				rm -rf ${appdir}/${app}/oat/${arch2}
			fi
		fi
	done
	deoappdir="${appdir}"
	if [[ ${dtype} != "l" ]]; then
		deoarch="oat/${arch}"
		deoarch2="${arch}"
		dodeodex
		if [[ ${arch2} ]]; then
			deoarch="oat/${arch2}"
			deoarch2="${arch2}"
			dodeodex
		fi
	else
		deoarch="${arch}"
		deoarch2="${arch}"
		dodeodex
		if [[ ${arch2} ]]; then
			deoarch="${arch2}"
			deoarch2="${arch2}"
			dodeodex
		fi
	fi
	echo -e "Starting deodex on ${BLUE}system/priv-app${NORMAL}..."
	applist=$(ls ${privdir})
	privapp=""
	for privapp in $(echo "${applist}"); do
		if [[ ${dtype} != "l" ]]; then
			if [[ ${arch2} && -d ${privdir}/${privapp}/oat/${arch} && -d ${privdir}/${privapp}/oat/${arch2} ]]; then
				rm -rf ${privdir}/${privapp}/oat/${arch2}
			fi
		else
			if [[ ${arch2} && -d ${privdir}/${privapp}/${arch} && -d ${privdir}/${privapp}/${arch2} ]]; then
				rm -rf ${privdir}/${privapp}/${arch2}
			fi
		fi
	done
	deoappdir="${privdir}"
	if [[ ${dtype} != "l" ]]; then
		deoarch="oat/${arch}"
		deoarch2="${arch}"
		dodeodex
		if [[ ${arch2} ]]; then
			deoarch="oat/${arch2}"
			deoarch2="${arch2}"
			dodeodex
			deoarch="oat/${arch}"
			deoarch2="${arch}"
		fi
	else
		deoarch="${arch}"
		deoarch2="${arch}"
		dodeodex
		if [[ ${arch2} ]]; then
			deoarch="${arch2}"
			deoarch2="${arch2}"
			dodeodex
			deoarch="${arch}"
			deoarch2="${arch}"
		fi
	fi
  echo -e "Starting deodex on ${BLUE}system/framework${NORMAL}...\n"
	cd "${framedir}" || exit
	if [[ ${dtype} = "n2" || ${dtype} = "m2" ]]; then
		if [[ ${dtype} = "n2" ]]; then
			ls ${deoarch2} | grep .oat$ | sort | while read line; do
				if [[ ${line} != "boot.oat" ]]; then
					framejar=$(echo "$(echo "${line}" | sed 's/^boot-//; s/\.oat$//').jar")
					if [[ $(${p7z} l ${framejar} | grep classes.dex) ]]; then
						echo "${GREEN}${frame} is already deodexed...${NORMAL}"
						continue
					fi
				fi
				echo -e "Deodexing ${GREEN}${line}${NORMAL}...\n"
				classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch2}/${line} | rev | cut -d"/" -f1 | rev)
				echo "${classes}" >> ${logs}/deodex-${logname}.log
				echo "${classes}" | while read line2; do
					if [[ ! $(echo "${line2}" | grep classes) ]]; then
						line3=$(echo "${line2}" | rev | cut -d"." -f2- | rev)
						dexclass="classes.dex"
					else
						line3=$(echo "${line2}" | rev | cut -d"." -f3- | rev)
						dexclass=$(echo "${line2}" | cut -d":" -f2-)
					fi
					java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${deoarch2}/boot.oat ${deoarch2}/${line}/${line2} -o ${deoarch2}/smali >> ${logs}/deodex-${logname}.log
					java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} ${deoarch2}/smali -o ${line3}.jar:${dexclass} >> ${logs}/deodex-${logname}.log
					rm -rf ${deoarch2}/smali
				done
			done
		elif [[ ${dtype} = "m2" ]]; then
			classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch2}/boot.oat | rev | cut -d"/" -f1 | rev)
			line=""
			for line in $(echo "${classes}"); do
				if [[ ! $(echo "${line}" | grep classes) ]]; then
					framejar="${line}"
					dexclass=":classes.dex"
				else
					framejar=$(echo "${line}" | cut -d":" -f1)
					dexclass=""
				fi
				if [[ $(${p7z} l ${framejar} | grep classes.dex) ]]; then
					echo -e "${GREEN}${frame}${NORMAL} is already deodexed...\n"
					continue
				fi
				line2=$(echo "${line}" | rev | cut -d"." -f2- | rev)
				echo -e "Deodexing ${GREEN}${line2}${NORMAL}...\n"
				java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${deoarch2}/boot.oat ${deoarch2}/boot.oat/${line} -o smali >> ${logs}/deodex-${logname}.log
				java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} smali -o ${line}${dexclass} >> ${logs}/deodex-${logname}.log
				rm -rf smali
			done
		fi
		if [[ -d ${deoarch} ]]; then
			frame=""
			for frame in $(ls ${deoarch} | grep .odex$ | sort); do
				framejar=$(echo "$(echo "${frame}" | rev | cut -d"." -f2- | rev).jar")
				if [[ $(${p7z} l ${framejar} | grep classes.dex) ]]; then
					echo -e "${GREEN}${frame}${NORMAL} is already deodexed...\n"
					continue
				fi
				echo -e "Deodexing ${GREEN}${frame}${NORMAL}...\n"
				classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch}/${frame})
				echo "${classes}" >> ${logs}/deodex.log
				echo "${classes}" | while read line; do
					apkdex=$(basename $(echo "${line}"))
					if [[ ! $(echo "${apkdex}" | grep classes) ]]; then
						dexclass="${apkdex}:classes.dex"
					else
						dexclass="${apkdex}"
					fi
					java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${deoarch2}/boot.oat ${deoarch}/${frame}/${apkdex} -o ${deoarch}/smali >> ${logs}/deodex-${logname}.log
					java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} ${deoarch}/smali -o ${dexclass} >> ${logs}/deodex-${logname}.log
					rm -rf ${deoarch}/smali
				done
			done
		fi
	elif [[ ${dtype} = "l" ]]; then
		if [[ -d ${deoarch} ]]; then
			for frame in $(ls ${deoarch} | grep .odex$ | sort); do
				echo -e "Deodexing ${GREEN}${frame}${NORMAL}...\n"
				errtest=$(echo "${frame}" | rev | cut -d"." -f2- | rev)
				java -Xmx${heapsize}m -jar ${oat2dex} ${deoarch}/${frame} ${deoarch2}/odex >> ${logs}/deodex-${logname}.log
			done
		fi
		mv ${deoarch2}/dex/* ${deoarch}/
		ls ${deoarch} | grep "\.dex$" | while read line; do
			if [[ ! $(echo "${line}" | grep classes) ]]; then
				frame=$(echo "$(echo "${line}" | sed 's/\.dex//').jar:classes.dex")
			else
				dexclass=$(echo "${line}" | rev | cut -d"-" -f1 | rev)
				frame=$(echo "$(echo "${line}" | sed "s/-${dexclass}//").jar:${dexclass}")
			fi
			mv ${deoarch}/${line} ${frame}
		done
	elif [[ ${dtype} = "o" ]]; then
		for d in ${framedir}/${arch} ${framedir}/${arch2}; do
			cd "${d}" || exit
			if [[ -f "boot.oat" && -f "boot.vdex" ]]; then
				bootclass=$(strings boot.oat | tr ' ' '\n' 2>/dev/null | grep -m 1 '\-\-dex\-location=' | cut -d'=' -f2-)
				if [[ ${bootclass} && ! -f ${bootclass}__classes.dex ]]; then
					mv boot.vdex boot-$(basename "${bootclass}" | sed 's/\.jar$/\.vdex/')
				fi
			fi
			find -name "boot-*.vdex" | while read i; do
				nname=$(echo "${i}" | sed 's/^\.\///; s/^boot-//; s/\.vdex$//')
				if [[ -f ${framedir}/${nname}.jar__classes.dex ]]; then
					continue
				fi
				echo -e "Deodexing ${GREEN}$(echo "$i" | sed 's/^\.\///')${NORMAL}...\n"
				mv "${i}" "${nname}.vdex"
				${vdexext} -i "${nname}.vdex" >> ${logs}/deodex-${logname}.log
				if [[ ! $(ls | grep "${nname}.apk_classes") ]]; then
					${vdexext} -i "${nname}.vdex" --ignore-crc-error >> ${logs}/deodex-${logname}.log
					if [[ ! $(ls | grep "${nname}.apk_classes") ]]; then
						echo "${d/$workdir/}/${nname}.jar" >> ${logs}/deodex_fail-${logname}.log
						continue
					else
						echo "${d/$workdir/}/${nname}.jar" >> ${logs}/deodex_crc_ignored-${logname}.log
					fi
				fi
				ls | grep "${nname}.apk" | while read r; do
					mv "${r}" "${framedir}/$(echo "${r}" | sed 's/apk_/jar:/')"
				done
			done
		done
		for d in ${framedir}/oat/${arch} ${framedir}/oat/${arch2}; do
			cd "${d}" || exit
			ls | grep ".vdex$" | while read i; do
				nname=$(echo "${i}" | sed 's/\.vdex$//')
				if [[ -f ${framedir}/${nname}.jar__classes.dex ]]; then
					continue
				fi
				echo -e "Deodexing ${GREEN}${i}${NORMAL}...\n"
				${vdexext} -i "${i}" >> ${logs}/deodex-${logname}.log
				if [[ ! $(ls | grep ".apk_classes") ]]; then
					${vdexext} -i "${i}" --ignore-crc-error >> ${logs}/deodex-${logname}.log
					if [[ ! $(ls | grep ".apk_classes") ]]; then
						echo "${d/$workdir/}/${nname}.jar" >> ${logs}/deodex_fail-${logname}.log
						continue
					else
						echo "${d/$workdir/}/${nname}.jar" >> ${logs}/deodex_crc_ignored-${logname}.log
					fi
				fi
				ls | grep "${nname}.apk" | while read r; do
					mv ${r} ${framedir}/$(echo "${r}" | sed 's/apk_/jar:/')
				done
			done
		done
	fi
	echo -e "Packing dex into jarfiles...\n"
    cd "${framedir}" || exit
	ls | grep "jar:classes.dex" | sort | while read line; do
        jname=$(echo "${line}" | cut -d':' -f1)
		ls | grep "^${jname}:" | while read i; do
            jclass=$(echo "${i}" | cut -d':' -f2)
			mv "${i}" "${jclass}"
		done
        if [[ -f "classes.dex" ]]; then
			(${aapt} add -fk "${jname}" classes*.dex 2>&1) >> ${logs}/deodex-${logname}.log
		else
			echo "ERROR: ${jname} has no classes.dex" >> ${logs}/deodex-${logname}.log
		fi
		rm -rf classes*.dex
	done
	if [[ ${dtype} = "l" || ${dtype} = "m" ]]; then
		rm -rf ${arch}/odex
		rm -rf ${arch}/dex
		if [[ ${arch2} ]]; then
			rm -rf ${arch2}/odex
			rm -rf ${arch2}/dex
		fi
	fi
	echo -e "Cleaning up...\n"
	if [[ -s ${logs}/extramv-${logname}.txt ]]; then
		cat ${logs}/extramv-${logname}.txt | while read line; do
			mv ${line}
			if [[ $(echo "${line}" | grep "app_chimera") ]]; then
				rm -rf $(dirname $(echo "${line}" | gawk '{print $1}'))
			fi
		done
	fi
	rm -rf oat
	rm -rf ${arch}
	if [[ ${arch2} ]]; then
		rm -rf ${arch2}
	fi

	if [[ ${dtype} = "l" ]]; then
		deodex_fail_list=$(grep "convertToDex: skip" ${logs}/deodex-${logname}.log | cut -d"/" -f2- | sort -u)
	else
		deodex_fail_list=$(cat ${logs}/deodex_fail-${logname}.log 2>/dev/null)
	fi
	if [[ ${deodex_fail_list} ]]; then
		echo -e "There were problems deodexing the following\nand will cause issues if you flash the ROM.\nYou have been warned."
		echo "${RED}${deodex_fail_list}${NORMAL}"
	fi
	cd ${workdir} || exit
	stillodexed=$(find system vendor -name *.odex 2>/dev/null)
	if [[ ! "${stillodexed}" ]]; then
		echo "Deodexing complete."
	else
		echo "The following odex files are still in your ROM:"
		echo "${YELLOW}${stillodexed}${NORMAL}"
	fi
  cd ${workdir} || exit
}
deodex_sqsh() {
	echo "Extracting ${sqshfile}..."
	if [[ $(echo "${sqshfile}" | grep "priv-app\|odex.app\|framework") ]]; then
		cd ${sysdir} || exit
		sqshtype=$(echo "${sqshfile}" | cut -d"." -f2)
		sqshdir=$(echo "sqshtmp/${sqshtype}")
	elif [[ $(echo "${sqshfile}" | grep "orig.applications") ]]; then
		cd ${sysdir}/etc/product || exit
		mkdir -p sqshtmp
		mv ${sqshfile} sqshtmp/
		cd sqshtmp || exit
		${p7z} x ${sqshfile} 2>/dev/null >> ${logs}/deodex-${logname}.log
		echo "Moving odex files..."
		if [[ $(ls | grep arm64) ]]; then
			odexarch="arm64"
			cd arm64 || exit
		elif [[ $(ls | grep arm) ]]; then
			odexarch="arm"
			cd arm || exit
		fi
		ls | while read line; do
			odexapp=$(echo "${line}" | sed 's/\.odex//')
			mv ${line} ${sysdir}/etc/product/applications/${odexapp}/oat/${odexarch}/
			mv ${sysdir}/etc/product/sqshtmp/${odexapp}/${odexapp}.apk ${sysdir}/etc/product/applications/${odexapp}/
		done
		rm -rf ${sysdir}/etc/product/sqshtmp ${sysdir}/etc/product/orig.applications
		sqshfile=""
		cd ${workdir} || exit
		return
	else
		sqshdir="sqshtmp"
	fi
	mkdir -p ${sqshdir}
	mv ${sqshfile} ${sqshdir}/
	cd ${sqshdir} || exit
	${p7z} x ${sqshfile} 2>/dev/null >> ${logs}/deodex-${logname}.log
	rm -rf ${sqshfile}
	echo "Moving odex files..."
	if [[ -d ${sysdir}/${sqshdir}/arm || -d ${sysdir}/${sqshdir}/arm64 ]]; then
		if [[ -d ${sysdir}/${sqshdir}/arm ]]; then
			cd ${sysdir}/${sqshdir}/arm || exit
			sqsharch="arm"
		elif [[ -d ${sysdir}/${sqshdir}/arm64 ]]; then
			cd ${sysdir}/${sqshdir}/arm64 || exit
			sqsharch="arm64"
		fi
		if [[ ! ${sqshtype} = "framework" ]]; then
			line=""
			ls | grep .odex | while read line; do
				tmpapp=$(echo "${line}" | sed 's/\.odex//')
				mkdir -p ${sysdir}/${sqshtype}/${tmpapp}/oat/${sqsharch}
				mv "${line}" ${sysdir}/${sqshtype}/${tmpapp}/oat/${sqsharch}/
			done
		else
			line=""
			ls | grep .odex | while read line; do
				tmpapp=$(echo "${line}" | sed 's/\.odex//')
				if [[ -d ${framedir}/${tmpapp} ]]; then
					mkdir -p ${framedir}/${tmpapp}/oat/${sqsharch}
					mv "${line}" ${framedir}/${tmpapp}/oat/${sqsharch}/
				fi
			done
			mkdir -p ${sysdir}/${sqshtype}/oat/${sqsharch}
			mv * ${sysdir}/${sqshtype}/oat/${sqsharch}/
		fi
	else
		cd ${sysdir}/sqshtmp || exit
		line=""
		find . -type d | sed 's/^.\///' | while read line; do
			mkdir -p ${sysdir}/${line}
		done
		line=""
		find . -type f | sed 's/^.\///' | while read line; do
			mv ${line} ${sysdir}/${line}
		done
	fi
	cd $sysdir || exit
	rmleft=$(echo "${sqshfile}" | sed 's/\.sqsh$//')
	rm -rf sqshtmp ${rmleft}
	sqshfile=""
	cd ${workdir} || exit
}

# Gets heapsize from system
get_heapsize(){
  if [[ -f ${tools}/heapsize ]]; then
    heapsize=$(cat ${tools}/heapsize)
  else
    heapsize=$(grep MemTotal /proc/meminfo | gawk '{ print $2/1024-500 }' | cut -d"." -f1)
  fi
}

# Deodex ROMs - Extract ROM zip to work with conversion and deodex.
extract_new(){
  header
  echo "Selected option: ${GREEN}Deodex ROMs${NORMAL}"
  echo "Current file: ${GREEN}${sourcezip}${NORMAL}"
  footer
  # Checks if zipfile contains a .dat format system image
  if [[ $(${p7z} l ${sourcezip} | grep system.new.dat) ]]; then
    # If the image exists on the zipfile, extract required files to work with
    echo "Extracting required images and firmwares..."
    ${p7z} x ${sourcezip} firmware-update *.new.dat* *.transfer.list *.img *.bin -o${workdir} 2>/dev/null >> ${logs}/deodex-${logname}.log
    cd ${workdir} || exit
    ls | grep "\.new\.dat" | while read i; do
      line=$(echo "${i}" | cut -d "." -f1)
      # If dat image is a xz compressed image, extracts it before conversion
      if [[ $(echo "${i}" | grep "\.dat\.xz") ]]; then
        echo "Unpacking ${i}..."
        ${p7z} e ${i} 2>/dev/null >> ${logs}/deodex-${logname}.log
        rm -f ${i}
      fi
      # If dat image is a brotli compressed image, extracts it before conversion
      if [[ $(echo "${i}" | grep "\.dat\.br") ]]; then
        echo "Unpacking ${i}..."
        ${brotli} -d ${i}
        rm -f ${i}
      fi
      # After extracting required images (if they're compressed) convert the dat format into RAW EXT4 img files
      echo "Converting ${line} dat to img..."
      ${sdat2img} ${line}.transfer.list ${line}.new.dat ${line}.img > ${logs}/deodex-${logname}.log
      # Remove residual files from conversion
      rm -rf ${line}.transfer.list ${line}.new.dat
    done
    romimg="system.img"
  else
    # If there's no image to work with, warn user and stop the job.
    echo "${RED}There is no system.new.dat in this ROM zip.${NORMAL}"
    echo "${RED}This conversor only works with .dat formats.${NORMAL}"
    return
  fi

  # Defines name for the partition output directory
  romimgdir="$(echo ${romimg} | rev | cut -d '.' -f2- | rev)"

  # Defines the path for the extracted image
  extractimg="${romimgdir}"

  # Extracts detected EXT4 images
  img_extract

  # If extracted images are residing in subfolders, set variables acordingly
  if [[ -f ${workdir}/system/init.rc && -d ${workdir}/system/system/app ]]; then
    sysdir="${workdir}/system/system"
  else
    sysdir="${workdir}/system"
  fi

  # If images were properly extracted, build.prop should be available. If it is, proceed with conversion
  if [[ -f ${workdir}/${romimgdir}/build.prop && ! ${romimgdir} = "system" && ! ${romimg} = "vendor.img" ]]; then
    mv ${workdir}/${romimgdir} ${sysdir}
  fi

  # Sets variables for framework, app and priv-app folders for deodexing
  framedir=${sysdir}/framework
  appdir=${sysdir}/app
  privdir=${sysdir}/priv-app

  # If build.prop is present on the system image, proceeds with extracting the remaining images
  if [[ -f ${sysdir}/build.prop ]]; then
    if [[ $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone") ]]; then
      for line in $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone"); do
        line2=$(echo "${line}" | sed 's/\.img//')
        extractimg="${line2}"
        img_extract
      done
    fi

    androidversion=$(grep "ro.build.version.release" ${sysdir}/build.prop | cut -d "=" -f2)
    api=$(grep "ro.build.version.sdk" ${sysdir}/build.prop | cut -d "=" -f2)

    get_devicename

    cp -R ${tools}/updater/META-INF/ ${workdir}/
    cp -R ${tools}/updater/install ${workdir}/
    link1=$(find system vendor -type l -printf "%l\n" 2> /dev/null | sed 's/^/symlink(\"/; s/$/\", /')
    link2=$(find system vendor -type l 2>/dev/null | sed 's/^/\"\//; s/$/\");/')
    paste -d '' <(echo "${link1}") <(echo "${link2}") | sort > ${projfiles}/symlinks
    if [[ $(grep "symlink(\"egl/" ${projfiles}/symlinks) ]]; then
      sed -i 's/symlink(\"egl/symlink(\"\/egl/' ${projfiles}/symlinks
    fi
    cp ${projfiles}/symlinks ${projfiles}/symlinks.orig
    find system -type l -exec rm -f {} \;

    update_project
  else
    echo -e "Failed to extract ROM file."
    read -n1 -r -p "Press any key to continue..."
    mainmenu
  fi
}

# Sepolicy patcher/injector
sepolicy_patch(){
  chosenimg2="boot.img"
  chosenimg="boot"
  unpack_boot
  patch_kernel
  sepol_patch
  repack_boot
}

patch_kernel(){
  # Go to Kernel RAMDisk directory to apply changes
  cd ${workdir}/${chosenimg}img/ramdisk || exit
  ${usesudo}sed -i "s/# secureboot\nservice setlockstate \/sbin\/setlockstate\n    class core\n    oneshot\n    seclabel u:r:vold:s0//g" init.miui.rc
  ${usesudo}sed -i "s/on property:sys.boot_completed=1\n    start mcd_service\n    start miui-post-boot/on property:sys.boot_completed=1\n    start mcd_service\n    start miui-post-boot\n    start restartc/g" init.miui.rc
  ${usesudo}sed -i "$ a service restartc  \/system\/bin\/sh \/sbin\/restart.sh boot_completed\n    class main\n    user root\n    seclabel u:r:init:s0\n    disabled\n    oneshot" init.miui.rc
  ${usesudo}cp ${tools}/restart.sh sbin/restart.sh
  ${usesudo}rm -rf sbin/setlockstate
  echo "Patching fstab..."
  # For each fstab file found, apply patches to remove forced encryption
  for fstab in fstab.*; do
    ${usesudo}sed -i "s/\b,verify\b//g" "${fstab}" 2>/dev/null
    ${usesudo}sed -i "s/fileencryption=ice,quota//g" "${fstab}" 2>/dev/null
    ${usesudo}sed -i "s/fileencryption=ice//g" "${fstab}" 2>/dev/null
    ${usesudo}sed -i "s/forceencrypt=/encryptable=/g" "${fstab}" 2>/dev/null
    ${usesudo}sed -i "s/forcefdeorfbe=/encryptable=/g" "${fstab}" 2>/dev/null
    ${usesudo}sed -i "s/fileencryption/encryptable/g" "${fstab}" 2>/dev/null
    ${usesudo}sed -i "s/ro,barrier/ro,noatime,barrier/g" "${fstab}" 2>/dev/null
  done
  echo "Patching default.prop..."
  # Removes specific props marked as default
  for props in default*.prop; do
    ${usesudo}sed -i "/ro.secureboot.devicelock/d" "${props}" 2>/dev/null
    ${usesudo}sed -i "/ro.bootimage.build.fingerprint/d" "${props}" 2>/dev/null
    ${usesudo}sed -i "/persist.sys.usb.config/d" "${props}" 2>/dev/null
    ${usesudo}sed -i "$ a persist.sys.usb.config=mtp,adb" "${props}" 2>/dev/null
  done
  cd ${workdir}/${chosenimg}img/split_img || exit
  export kdevice="$(cat ${workdir}/system/build.prop | grep ro.product.device= | cut -d "=" -f2)"
  echo "Device: ${kdevice}"
  echo "Patching kernel cmdline..."
  if [[ ${kdevice} == "sagit" ]]; then
    echo "Mi 6"
    ${usesudo}sed -i "s/$/ androidboot.verifiedbootstate=green androidboot.selinux=permissive/" boot.img-cmdline
  else
    ${usesudo}sed -i "s/$/ androidboot.verifiedbootstate=green androidboot.flash.locked=1 androidboot.selinux=permissive/" boot.img-cmdline
  fi
  if [[ ${kdevice} == "meri" || ${kdevice} == "song" ]]; then
    ${usesudo}rm -rf ${workdir}/${chosenimg}img/split_img/*sigtype
    ${usesudo}rm -rf ${workdir}/${chosenimg}img/split_img/*avbtype
  fi
  cd ${workdir} || exit
}

# Unpacks boot image
unpack_boot(){
  if [[ -f ${chosenimg2} ]]; then
    mkdir -p ${workdir}/${chosenimg}img
    cd ${bin}/kerneltools || exit
    echo "Unpacking kernel..."
    ${usesudo}./unpackimg.sh ${workdir}/${chosenimg2} 2>&1 >> ${logs}/deodex-${logname}.log
    if [[ ${?} = "1" ]]; then
      rm -rf ${workdir}/${chosenimg}img ramdisk split_img
      echo "Error unpacking boot."
      bootext=""
      cd ${workdir} || exit
      return
    fi
    ${usesudo}mv ramdisk ${workdir}/${chosenimg}img/
    ${usesudo}mv split_img ${workdir}/${chosenimg}img/
  else
    echo "Boot image wasn't found on the working directory"
    bootext=""
    cd ${workdir} || exit
    return
  fi
  bootext=""
  cd ${workdir} || exit
}

# Patches sepolicy on unpacked boot image
sepol_patch(){
  sepol=$(${usesudo}find -name *sepolicy* 2>/dev/null | grep -v "test\|txt\|mapping")
  if [[ ${sepol} ]]; then
    sepgood=""
    while read i; do
      omd5=$(md5sum "${i}" | gawk '{print $1}')
      if [[ $(echo "${i}" | grep "\.cil$") ]]; then
        line=$(grep -e "(allow zygote.* dalvikcache_data_file.* (file (" "${i}")
        if [[ ${line} ]]; then
          while read e; do
            if [[ ! $(echo "${e}" | grep "execute") ]]; then
              nline=$(echo "${e}" | sed 's/)))/\ execute)))/')
              sed -i "s/${e}/${nline}/" "${i}"
            fi
          done <<< "${line}"
        else
          znames=$(grep -e "(allow zygote.*" "${i}" | cut -d' ' -f2 | sort -u)
          dnames=$(grep -e "(allow .*dalvikcache_data_file.*" "${i}" | cut -d' ' -f3 | sort -u)
          if [[ ${znames} && ${dnames} ]]; then
            while read z; do
              while read d; do
                echo "(allow ${z} ${d} (file (execute)))" >> "${i}"
              done <<< "${dnames}"
            done <<< "${znames}"
          fi
        fi
      else
        dumpf=$(${usesudo}${bin}/kerneltools/bootimg seinject -dt -P "${i}")
        znames=$(grep -e "ALLOW zygote.*" <<< "${dumpf}" | cut -d' ' -f3 | sort -u)
        dnames=$(grep -e "ALLOW.*dalvikcache_data_file.*" <<< "${dumpf}" | cut -d' ' -f5 | sort -u)
        if [[ ${znames} && ${dnames} ]]; then
          while read z; do
            while read d; do
              ${usesudo}${bin}/kerneltools/bootimg seinject -s ${z} -t ${d} -c file -p execute -P "${i}" -o "${i}" >> ${logs}/deodex-${logname}.log
            done <<< "${dnames}"
          done <<< "${znames}"
        else
          ${usesudo}${bin}/kerneltools/bootimg seinject -s zygote -t dalvikcache_data_file -c file -p execute -P "${i}" -o "${i}" >> ${logs}/deodex-${logname}.log
        fi
      fi
      if [[ $(md5sum "${i}" | gawk '{print $1}') != ${omd5} ]]; then
        sepgood=1
        ${usesudo}chown ${whoiam}:${whoiam} "${i}"
      fi
    done <<< "${sepol}"
    if [[ ${sepgood} ]]; then
      echo "Kernel sepolicy patched."
    else
      echo "Failed patching kernel sepolicy."
    fi
  else
      echo ""
  fi
}

# Repacks boot image
repack_boot(){
  if [[ ! -d ${projfiles}/${chosenimg}_orig ]]; then
    mkdir -p ${projfiles}/${chosenimg}_orig
    cp ${workdir}/${chosenimg2} ${projfiles}/${chosenimg}_orig/
  fi
  ${usesudo}mv ${workdir}/${chosenimg}img/ramdisk ${bin}/kerneltools/
  ${usesudo}mv ${workdir}/${chosenimg}img/split_img ${bin}/kerneltools/
  cd ${bin}/kerneltools || exit
  echo "Repacking kernel..."
  ${usesudo}./repackimg.sh 2>&1 >> ${logs}/deodex-${logname}.log
  if [[ ${?} = "1" ]]; then
    ${usesudo}mv ${bin}/kerneltools/ramdisk ${workdir}/${chosenimg}img/
    ${usesudo}mv ${bin}/kerneltools/split_img ${workdir}/${chosenimg}img/
    echo "${RED}Failed repacking kernel image${NORMAL}"
  else
    ${usesudo}chown -hR ${whoiam}:${whoiam} image-new.img
    cp image-new.img ${workdir}/${chosenimg}.img
    ${verityoff} ${workdir}/${chosenimg}.img
    ${usesudo}./cleanup.sh 2>&1 >> ${logs}/deodex-${logname}.log
    rm -rf ${workdir}/${chosenimg}img
  fi
  cd ${workdir} || exit
  return
}

# Update project files
update_project(){
  permtype="set_metadata"
  romdir2=$(echo "${workdir}" | sed 's:\/:\\/:g')
  find ${sysdir} ${workdir}/vendor 2>/dev/null | sed "s/${romdir2}//g" | sort > ${projfiles}/all_files.txt
  cd ${projfiles} || exit

  rm -rf set_metadata
  cp set_metadata1 set_metadata

  if [[ -s root_meta ]]; then
    cat root_meta >> set_metadata
  fi

  rm -rf symlinks
  cp symlinks.orig symlinks
  cp ${tools}/updater/custom/radio/${devicename} ${projfiles}/radioscript
  grep "/system/app\|/system/priv-app" all_files.txt | cut -d"/" -f1-4 | grep -v "^/system/app$\|^/system/priv-app$" | sort -u >> appsym
	line=""
	grep "/system/app" symlinks | cut -d"\"" -f4 | cut -d"/" -f1-4 | while read line; do
		if [[ ! "${line}" = $(grep "^${line}$" appsym) ]]; then
			grep -v "${line}" symlinks > symlinks2
			mv symlinks2 symlinks
		fi
	done
  line=""
	grep "/system/priv-app" symlinks | cut -d"\"" -f4 | cut -d"/" -f1-4 | while read line; do
		if [[ ! "${line}" = $(grep "^${line}$" appsym) ]]; then
			grep -v "${line}" symlinks > symlinks2
			mv symlinks2 symlinks
		fi
	done
	sort -u symlinks > symlinks2
	mv symlinks2 symlinks
	rm -rf appsym

  devname1=$(echo "${devicename}" | sed 's/\ /\\ /g')
  if [[ ! -f assert ]]; then
    cat ${tools}/updater/custom/assert >> assert
    cat ${tools}/updater/custom/abort >> assert
    sed -i "s/#DEVICENAME/${devname1}/g" assert
    sed -i "s/#DEVICECHK/${devicechk}/g" assert
    if [[ -f assertcustom ]]; then
      grep "ro.product.device" assert > assert-2
      mv assert-2 assert
      cat assertcustom >> assert
    fi
  fi
  if [[ ! $(grep " getprop(\|(getprop(" ${updatedir}/updater-script) ]]; then
    sed -i '/#ASSERT/ r assert' ${updatedir}/updater-script
  fi
  sed -i "s/#DEVICENAME/${devname1}/g" ${updatedir}/updater-script
  sed -i "s/#DEVICECHK/${devicechk}/g" ${updatedir}/updater-script
  sed -i '/#SYM/ r symlinks' ${updatedir}/updater-script
  if [[ -f ${projfiles}/set_metadataV ]]; then
    cat ${projfiles}/set_metadataV >> ${permtype}
  fi
  sed -i "/#PERM/ r ${permtype}" ${updatedir}/updater-script
  sed -i "/#RADIO/ r radioscript" ${updatedir}/updater-script
  grep -v "#PERM\|#SYM\|#ASSERT\|#RADIO" ${updatedir}/updater-script > ${updatedir}/updater-script2
  mv ${updatedir}/updater-script2 ${updatedir}/updater-script
  rm -rf ${projfiles}/exdirs

  cd ${workdir} || exit
  if [[ $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone") ]]; then
    for part in $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone"); do
      eximg=$(echo "${part}" | sed 's/\.img//')
      echo "${eximg}" >> ${projfiles}/exdirs
    done
    for line in $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone"); do
      line2=$(echo "${line}" | sed 's/\.img//')
      whatimg="${line2}"
      add_partition
    done
  fi
  cd ${workdir} || exit
  rm -rf system.img vendor.img
}

# Create partition entrypoints on updater-script if they exist
add_partition(){
  cd ${updatedir} || exit
  gawk 'a=/^mount/{b=1}b&&!a{print "#MOUNT";b=0}1' updater-script > updater-script2
  mv updater-script2 updater-script
  sed -i "s/#MOUNT/mount(\"ext4\"\,\ \"EMMC\"\,\ file_getprop\(\"\/tmp\/config\",\ \"${whatimg}\"\),\ \"\/${whatimg}\");/" updater-script
  gawk 'a=/^ifelse\(is_mounted/{b=1}b&&!a{print "#UNMOUNT1";b=0}1' updater-script > updater-script2
  mv updater-script2 updater-script
  gawk 'a=/^unmount/{b=1}b&&!a{print "#UNMOUNT2";b=0}1' updater-script > updater-script2
  mv updater-script2 updater-script
  sed -i "s/#UNMOUNT1/ifelse(is_mounted(\"\/${whatimg}\"),\ unmount(\"\/${whatimg}\"));/" updater-script
  sed -i "s/#UNMOUNT2/unmount(\"\/${whatimg}\");/" updater-script
  if [[ ! ${whatimg} = "data" ]]; then
    gawk 'a=/^format/{b=1}b&&!a{print "#FORMAT";b=0}1' updater-script > updater-script2
    mv updater-script2 updater-script
    sed -i "s/#FORMAT/format(\"ext4\"\,\ \"EMMC\"\,\ file_getprop\(\"\/tmp\/config\",\ \"${whatimg}\"\),\ \"0\"\,\ \"\/${whatimg}\");/" updater-script
  fi
  if [[ -s ${projfiles}/exdirs && $(grep "${whatimg}" ${projfiles}/exdirs) ]]; then
    partup=$(echo ${whatimg} | gawk '{print toupper($0)}')
    if [[ ! $(grep "#${partup}" updater-script) ]]; then
      line=$(grep "package_extract_dir(\"system\"" updater-script | sed 's/\"/\\"/g; s:\/:\\/:g')
      sed -i "s:${line}:${line}\n#${partup}:" updater-script
    fi
    if [[ ${whatimg} = "vendor" ]]; then
      sed -i "/#${partup}/ r ${tools}/updater/vendor-set_metadata.txt" updater-script
    else
      sed -i "/#${partup}/ r ${tools}/updater/extra-set_metadata.txt" updater-script
    fi
    sed -i "s/#PEXTRA/${whatimg}/g" updater-script
    touch ${projfiles}/${whatimg}img
    if [[ ${whatimg} = "vendor" && -f ${projfiles}/set_metadataV ]]; then
      cat ${projfiles}/set_metadataV >> ${projfiles}/set_metadata
    fi
  elif [[ ${whatimg} = "data" ]]; then
    gawk 'a=/\"system\",/{b=1}b&&!a{print "#DATA";b=0}1' updater-script > updater-script2
    mv updater-script2 updater-script
    sed -i "/#DATA/ r ${tools}/updater/data-set_metadata.txt" updater-script
    touch ${projfiles}/data-set_metadata
  fi
  whatimg=""
  cd ${workdir} || exit
}

# Gets devicename
get_devicename(){
  devicename=""
  while read i; do
    if [[ ! -f ${i} ]]; then
      continue
    fi
    while read x; do
      if [[ $(grep "${x}=" ${i}) ]]; then
        devicename="$(grep -m 1 "${x}=" ${i} | cut -d"=" -f2)"
        devicechk="${x}"
        break
      fi
    done <<< "$(echo -e "ro.product.device\nro.build.product\nro.product.name")"
  done <<< "$(echo -e "${sysdir}/build.prop\n${workdir}/build.prop")"
  if [[ ! "${devicename}" ]]; then
    while read i; do
      if [[ ! -f ${i} ]]; then
        continue
      fi
      devicename="$(grep -m 1 "ro.build.description=" ${i} | cut -d"=" -f2 | cut -d'-' -f1)"
      devicechk="ro.product.device"
      break
    done <<< "$(echo -e "${sysdir}/build.prop\n${workdir}/build.prop")"
  fi
}

# Extracts partition images to work from
img_extract(){
  # Checks if the image is in sparse format. If it is, convert it.
  ${simg2img} ${extractimg}.img ${extractimg}.img-2 2>/dev/null
  if [[ ! -s ${extractimg}.img-2 ]]; then
    rm -rf ${extractimg}.img-2
  else
    mv ${extractimg}.img-2 ${extractimg}.img
  fi
  mkdir -p output
  # Mounts the partition to copy files from
  echo "Mounting ${extractimg} image..."
  ${usesudo}mount -o loop -t ext4 ${extractimg}.img output/
  mkdir -p ${extractimg}; mkdir -p ${projfiles}
  # Copies all files from source partition to a destination folder to be used during conversion
  # Sets all permissions accordingly and changes it's ownership to the current user
  echo "Copying ${extractimg} contents..."
  ${usesudo}cp -R output/* ${extractimg}/
  echo "Taking ownership of ${extractimg} files..."
  ${usesudo}chown -hR ${whoiam}:${whoiam} ${workdir}/${extractimg}
  echo "Setting up permissions on ${extractimg} files..."
  ${usesudo}chmod -R a+rwX ${workdir}/${extractimg}
  apitmp1=$(find system -name build.prop 2>/dev/null | grep -m 1 "")
  if [[ -f ${apitmp1} ]]; then
    apitmp=$(grep "ro.build.version.sdk" "${apitmp1}" | cut -d "=" -f2)
  else
    apitmp=""
  fi
  # If ROM API is greater than 19, proceeds with conversion.
  if [[ ${apitmp} -ge "19" ]]; then
    echo "Extracting metadatas from ${extractimg}..."
    local getmeta=$(${getmetadata} -s "${workdir}/output" "${extractimg}")
    if [[ ${extractimg} = "system" || ${extractimg} = "vendor" ]]; then
      if [[ ${extractimg} = "system" ]]; then
        if [[ -f output/init.rc && -d output/system/app ]]; then
          echo "${getmeta}" | grep -v "system/system\|system/ " | while read i; do
            local thefile=$(echo "${i}" | gawk '{print $1}')
						local uid=$(echo "${i}" | gawk '{print $2}')
						local gid=$(echo "${i}" | gawk '{print $3}')
						local mode=$(echo "${i}" | gawk '{print $4}')
						local cap=$(echo "${i}" | gawk '{print $5}')
						local con=$(echo "${i}" | gawk '{print $6}')
						echo "set_metadata(\"/${thefile}\", \"uid\", ${uid}, \"gid\", ${gid}, \"mode\", ${mode}, \"capabilities\", ${cap}, \"selabel\", \"${con}\");" >> ${projfiles}/root_meta
					done
          # Searches for relevant files to get it's correct metadata
					find output/system/bin -type f 2>/dev/null | sed 's/^output/system/' | sort > ${projfiles}/binblk
					find output/system/vendor/bin -type f 2>/dev/null | sed 's/^output/system/' | sort > ${projfiles}/vinblk
					cp ${tools}/updater/set_meta_list2 ${projfiles}/set_meta_list
				else
					find output/bin -type f 2>/dev/null | sed 's/^output/system/' | sort > ${projfiles}/binblk
					find output/vendor/bin -type f 2>/dev/null | sed 's/^output/system/' | sort > ${projfiles}/vinblk
					cp ${tools}/updater/set_meta_list ${projfiles}/set_meta_list
				fi
        # Apply all the metadata found from files on the source partition to the preliminary updater-script
        sed -i "/#BIN/ r ${projfiles}/binblk" ${projfiles}/set_meta_list
        sed -i "/#VBIN/ r ${projfiles}/vinblk" ${projfiles}/set_meta_list
        rm -rf ${projfiles}/binblk ${projfiles}/vinblk
        metalist=$(cat ${projfiles}/set_meta_list)
        # If there's a vendor image, also grab it's metadata
      elif [[ ${extractimg} = "vendor" ]]; then
        if [[ ${getmeta} ]]; then
          metalist=$(echo "${getmeta}" | grep "^vendor/bin" | gawk '{print $1}' | sort)
        else
          metalist=$(cat ${tools}/updater/set_meta_listV)
        fi
      fi
      echo "${metalist}" > ${projfiles}/tmpmetalist
      echo "${getmeta}" | grep -v ":system_file\|:system_library_file\|:vendor_file\|vendor_configs_file\|vendor_app_file\|vendor_hal_file\|vendor_overlay_file\|vendor_framework_file\|same_process_hal_file\|:rootfs" | sort | while read line; do
        i=$(echo "${line}" | gawk '{print $1}')
        if [[ ! $(grep "${i}" ${projfiles}/tmpmetalist) ]]; then
          echo "${i}" >> ${projfiles}/tmpmetalist
        fi
      done
      cat ${projfiles}/tmpmetalist | while read line; do
        if [[ ${extractimg} = "system" ]]; then
          set_metadata1="set_metadata1"
        else
          set_metadata1="set_metadataV"
        fi
        if [[ $(echo "${line}" | grep set_metadata) ]]; then
          ftmp=$(echo "${line}" | cut -d'"' -f2 | sed 's:^\/::')
          if [[ -f ${ftmp} || -d ${ftmp} ]]; then
            echo "${line}" >> ${projfiles}/${set_metadata1}
          fi
        elif [[ -f ${line} ]]; then
          local i=$(grep "^${line} " <<< "${getmeta}")
          local thefile=$(echo "${i}" | gawk '{print $1}')
          local uid=$(echo "${i}" | gawk '{print $2}')
          local gid=$(echo "${i}" | gawk '{print $3}')
          local mode=$(echo "${i}" | gawk '{print $4}')
          local cap=$(echo "${i}" | gawk '{print $5}')
          local con=$(echo "${i}" | gawk '{print $6}')
          echo "set_metadata(\"/${thefile}\", \"uid\", ${uid}, \"gid\", ${gid}, \"mode\", ${mode}, \"capabilities\", ${cap}, \"selabel\", \"${con}\");" >> ${projfiles}/${set_metadata1}
        fi
      done
    fi
  fi
  echo "Umounting ${extractimg} image..."
  ${usesudo}umount output/
  echo "Removing unneeded files..."
  rm -rf ${workdir}/output ${projfiles}/cap_temp ${projfiles}/set_meta_list ${projfiles}/tmpmetalist
  extractimg=""
}

check_deps
check_user
cleanup
mainmenu
