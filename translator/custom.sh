#!/bin/bash

echo "Patching build.prop..."
cd ${cdir}/translator/projects/input.zip.bzprj/baseROM/system
rommoddevice=$(cat build.prop | grep ro.product.device= | cut -d "=" -f2)
sed -i "/ro.bootimage.build.fingerprint=/d" ${PWD}/etc/prop.default
sed -i "/ro.build.version.incremental=/d" build.prop
sed -i "$ a ro.build.version.incremental=${romversion}" build.prop
if [[ "${rombase}" == "4.4" ]]; then
  sed -i '$ a ro.com.google.gmsversion=4.4_r4' build.prop
elif [[ "${rombase}" == "5.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=5.0_r5' build.prop
elif [[ "${rombase}" == "5.1" ]]; then
  sed -i '$ a ro.com.google.gmsversion=5.1_r7' build.prop
elif [[ "${rombase}" == "6.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=6.0_r2' build.prop
elif [[ "${rombase}" == "7.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=7.0_r3' build.prop
  sed -i '$ a ro.setupwizard.require_network=any' build.prop
elif [[ "${rombase}" == "7.1" ]]; then
  sed -i '$ a ro.com.google.gmsversion=7.1_r3' build.prop
  sed -i '$ a ro.setupwizard.require_network=any' build.prop
elif [[ "${rombase}" == "8.0" ]]; then
  sed -i '$ a ro.com.google.gmsversion=8.0_r3' build.prop
  sed -i '$ a ro.setupwizard.require_network=any' build.prop
elif [[ "${rombase}" == "8.1" ]]; then
  sed -i '$ a ro.com.google.gmsversion=8.1_r1' build.prop
  sed -i '$ a ro.setupwizard.require_network=any' build.prop
fi

export epochdate="$(date +%s)"
export utcdate="$(date -u -d @${epochdate})"
sed -i "$ a ro.build.date=${utcdate}" build.prop
sed -i "$ a ro.build.date.utc=${epochdate}" build.prop

echo "Patching forceencrypt..."
if [[ -d ${cdir}/translator/projects/input.zip.bzprj/baseROM/system/vendor/etc ]]; then
  cd ${cdir}/translator/projects/input.zip.bzprj/baseROM/system/vendor/etc
  ls fstab* | while read fstabby; do
    sed -i "s/fileencryption=ice,quota//g" ${cdir}/translator/projects/input.zip.bzprj/baseROM/system/vendor/etc/${fstabby}
  done
fi
if [[ -d ${cdir}/translator/projects/input.zip.bzprj/baseROM/vendor/etc ]]; then
  cd ${cdir}/translator/projects/input.zip.bzprj/baseROM/vendor/etc
  ls fstab* | while read fstabby; do
    sed -i "s/fileencryption=ice,quota//g" ${cdir}/translator/projects/input.zip.bzprj/baseROM/vendor/etc/${fstabby}
  done
fi

echo "Fixing Settings.apk..."
cd ${cdir}/translator/projects/input.zip.bzprj/baseROM/system/priv-app/Settings
unzip -q Settings.apk -d unzipped
cd unzipped
zip -0qr Settings.zip *
rm -rf ../Settings.apk
mv Settings.zip ../Settings.apk
cd ..
rm -rf unzipped
